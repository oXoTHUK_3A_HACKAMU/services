﻿using UnityEngine;
using UnityEngine.Purchasing;

public class IAPService : MonoBehaviour, IStoreListener
{
    public static IAPService iapServiceInstance = null;
    private static IStoreController storeController = null;
    private static IExtensionProvider extentionProvider = null;

    [SerializeField] private string points1000 = "POINTS 1000";
    private void Awake()
    {
        if (iapServiceInstance == null)
            iapServiceInstance = this;
        else
            Destroy(this.gameObject);
    }
    public void BuyPoints1000()
    {
        BuyProductId(points1000);
    }
    private void BuyProductId(string productId)
    {
        if (IsInitialized())
        {
            var product = storeController.products.WithID(productId);
            if(product != null && product.availableToPurchase)
            {
                storeController.InitiatePurchase(product);
            }
        }
        else
        {
#if UNITY_EDITOR
            Debug.Log("Method BuyProductId failed. Service not initialized");
#endif
        }
    }

    public void InitializePurchsing()
    {
        if (IsInitialized()) return;

        var iapBuilder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

        iapBuilder.AddProduct(points1000, ProductType.Consumable);

        UnityPurchasing.Initialize(this, iapBuilder);
    }

    private bool IsInitialized()
    {
        return (storeController != null && (extentionProvider != null));
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
#if UNITY_EDITOR
        Debug.Log("IAP Initialization failed with error : " + error);
#endif
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs purchaseEvent)
    {
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
#if UNITY_EDITOR
        Debug.Log($"Purchase failed {product.definition.storeSpecificId}, Error {failureReason}");
#endif
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        storeController = controller;
        extentionProvider = extensions;

#if UNITY_EDITOR
        Debug.Log("IAP INITIALIZED");
#endif
    }
}