﻿using GameAnalyticsSDK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    public Text text;
    public Slider slider;

    int value;

    public void Tap()
    {
        slider.value += 0.1f;
        if(slider.value >= slider.maxValue)
        {
            slider.maxValue = slider.maxValue * 3;
            slider.value = 0;
            value++;
            text.text = "Level : " + value;
            GAManager.instance.Check(value);
        }
    }
}
