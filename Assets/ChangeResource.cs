﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Advertisements;

public class ChangeResource : MonoBehaviour
{
    public Text Gold;
    public Text House;
    public Text People;

    [SerializeField]int count = 1;
    [SerializeField]int enemy = 1;
    private void Start()
    {
        if (Advertisement.isSupported)
        {
            Advertisement.Initialize("3898129", false);
        }
        StartCoroutine(ChangeResources());
        StartCoroutine(Enemy());
    }
    public void BuyHouse()
    {
        if (ResourceType.instance.Gold >= 100)
        {
            ResourceType.instance.Gold -= 100;
            ResourceType.instance.House++;
        }
    }
    public void AddGold100()
    {
        ResourceType.instance.Gold += 100;
    }
    public void AddGold1000()
    {
        ResourceType.instance.Gold += 1000;
    }

    IEnumerator ChangeResources()
    {
        while (true)
        {
            ResourceType.instance.Gold += 5 * ResourceType.instance.House;
            ResourceType.instance.People += 2 * ResourceType.instance.House;
            UpdateText();
            yield return new WaitForSeconds(1f);
        }
    }

    private void UpdateText()
    {
        Gold.text = "Золота : " + ResourceType.instance.Gold;
        House.text = "Домов : " + ResourceType.instance.House;
        People.text = "Людей : " + ResourceType.instance.People;
    }

    IEnumerator Enemy()
    {
        while (true)
        {
            yield return new WaitForSeconds(5f);
            ResourceType.instance.People -= enemy;
            enemy += 3;
            count++;
            if (count >= 3)
            {
                count = 1;
                if (Advertisement.IsReady())
                {
                    Advertisement.Show(("video"));
                    Debug.Log("Реклама");
                }
            }
        }
    }
}
